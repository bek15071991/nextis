﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.Annotations;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Core
{
    public interface IMessageRepository : IRepository<Message>
    {
        Task<IReadOnlyCollection<Message>> Get([NotNull] ISpecification<Message> specification);

        Task Add([NotNull] Message message);
    }
}