﻿using System;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Core
{
    public class ByAuthorWithAuthorSpecification : Specification<Message>
    {
        public ByAuthorWithAuthorSpecification(long authorId) : base(message => message.AuthorId == authorId)
        {
            AddInclude(message => message.Author);
        }
    }

    public class DateTimeRangeWithAuthorSpecification : Specification<Message>
    { 
        public DateTimeRangeWithAuthorSpecification(DateTime from, DateTime to):base(message => message.OnDate >= from && message.OnDate <= to)
        {
            AddInclude(message => message.Author);
        } 
    }

    public static class MessageSpecifications
    {
        public static Specification<Message> ByAuthorWithAuthor(long userId)
        {
            return new ByAuthorWithAuthorSpecification(userId);
        }

        public static Specification<Message> RangeWithAuthor(DateTime from, DateTime to)
        {
            return new DateTimeRangeWithAuthorSpecification(from, to);
        }
    }
}