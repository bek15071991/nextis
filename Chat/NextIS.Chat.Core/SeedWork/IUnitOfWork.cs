﻿using System;

namespace NextIS.Chat.Core.SeedWork
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}