﻿using System;

namespace NextIS.Chat.Core.SeedWork
{
    public abstract class DateTimeProvider
    {
        private static DateTimeProvider _instance;

        public static DateTimeProvider Instance
        {
            get => _instance ?? (_instance = new DefaultDateTimeProvider());
            set => _instance = value ?? throw new ArgumentNullException("DateTimeProvider cannot be null");
        }

        public static DateTime Now => Instance.GetNow();

        static DateTimeProvider()
        {
            Instance = new DefaultDateTimeProvider();
        }

        public abstract DateTime GetNow();

        private class DefaultDateTimeProvider : DateTimeProvider
        {
            public override DateTime GetNow()
            {
                return DateTime.UtcNow;
            }
        }
    }
}