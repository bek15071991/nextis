﻿using JetBrains.Annotations;

namespace NextIS.Chat.Core.SeedWork
{
    public interface IUnitOfWorkFactory
    {
        [NotNull]
        IUnitOfWork Create();
    }
}