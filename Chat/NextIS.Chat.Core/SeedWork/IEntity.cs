﻿namespace NextIS.Chat.Core.SeedWork
{
    public interface IEntity
    {
        long Id { get; }
    }
}