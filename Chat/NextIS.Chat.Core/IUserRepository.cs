﻿using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.Annotations;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Core
{
    public interface IUserRepository : IRepository<User>
    {
        [ItemCanBeNull]
        Task<User> Get([NotNull] string login, [NotNull] string password);
        
        Task<IReadOnlyCollection<User>> Get();
    }
}