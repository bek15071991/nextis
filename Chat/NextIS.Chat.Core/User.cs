﻿using System;
using JetBrains.Annotations;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Core
{
    public class User : IEntity
    {
        public long Id { get; private set; }

        public string Login { get; private set; }

        public string Password { get; private set; }

        public User(long id, [NotNull] string login, [NotNull] string password)
        {
            Id = id;
            if (string.IsNullOrEmpty(login))
                throw new ArgumentNullException(nameof(login));
            Login = login;

            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException(nameof(password));

            Password = password;
        }

        public User([NotNull] string login, [NotNull] string password) : this(0, login, password)
        {
        }
    }
}