﻿using System;
using JetBrains.Annotations;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Core
{
    public class Message : IEntity
    {
        public long Id { get; private set; }

        public string Text { get; private set; }

        public DateTime OnDate { get; private set; }

        public long AuthorId { get; private set; }
        public User Author { get; private set; } 
        private Message()
        { }

        public Message(long userId, [NotNull] string text)
        {
            if (string.IsNullOrEmpty(text))
                throw new ArgumentNullException(nameof(text));

            AuthorId = userId;
            Text = text;

            OnDate = DateTimeProvider.Now;
        }
    }
}