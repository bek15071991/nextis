﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using NextIS.Chat.Core;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Web
{
    [Authorize]
    public class ChatHub : Hub
    {
        private  static readonly ConcurrentBag<string> _users = new ConcurrentBag<string>();

        private readonly ILogger<ChatHub> _logger;

        private readonly IMessageRepository _messageRepository;
        private readonly IUnitOfWorkFactory _unitOfWork;

        public ChatHub(
            [NotNull] ILogger<ChatHub> logger,
            [NotNull] IMessageRepository messageRepository,
            [NotNull] IUnitOfWorkFactory unitOfWork)
        {
            _messageRepository = messageRepository ?? throw new ArgumentNullException(nameof(messageRepository));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Send(string messageText, string userName)
        {
            if (string.IsNullOrEmpty(messageText) ||
                string.IsNullOrEmpty(userName))
                return;

            try
            {
                _logger.LogInformation($"{userName}: {messageText}");

                using (var uow = _unitOfWork.Create())
                {
                    var userId = GetCurrentUserId();

                    var message = new Message(userId, messageText);

                    await _messageRepository.Add(message);

                    await Clients.All.SendAsync("Send", messageText, userName);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "$Cannot send message :{0} from {1}", messageText, userName);
            }
        }

        public override async Task OnConnectedAsync()
        {
            var userName = GetUserName();

            await Clients.Others.SendAsync("Login", userName);
             
            if (EnumerableExtensions.Any(_users))
                await Clients.Caller.SendAsync("OnlineUsers", _users.ToArray());

            _users.Add(userName);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var userName = GetUserName();

            await Clients.Others.SendAsync("Logout", userName);

            _users.TryTake(out userName);

            _logger.LogError(exception, "$Cannot on disconnect user:{0}", userName);
            await base.OnDisconnectedAsync(exception);
        }

        private string GetUserName()
        {
            var user = Context.User;
            var userIdentity = user.Identity;
            return userIdentity.Name;
        }

        private long GetCurrentUserId()
        {
            var user = Context.User;
            var userIdentity = (ClaimsIdentity) user.Identity;

            var id = userIdentity.FindFirst(ClaimTypes.Sid).Value;

            return Convert.ToInt64(id);
        }
    }
}