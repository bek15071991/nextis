﻿using System;
using System.Collections.Generic;
using NextIS.Chat.Core;

namespace NextIS.Chat.Web.Models.Chat
{
    public class MessageFilterModel
    {
        public long? AuthorId { get; set; }
         
        public DateTime? From { get; set; }

        public DateTime? To { get; set; }

        public bool IsEmpty => AuthorId == null && From == null && To == null;

        public IReadOnlyCollection<User> Authors { get; set; } = new List<User>();

        public IReadOnlyCollection<Message> Messages { get; set; } = new List<Message>();
    }
}
