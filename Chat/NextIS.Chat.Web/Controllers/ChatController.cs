﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NextIS.Chat.Core;
using NextIS.Chat.Core.SeedWork;
using NextIS.Chat.Web.Models.Chat;

namespace NextIS.Chat.Web.Controllers
{
    [Authorize]
    public class ChatController : Controller
    {
        private readonly IMessageRepository _messageRepository;
        private readonly IUserRepository _userRepository;

        public ChatController([NotNull] IMessageRepository messageRepository, [NotNull] IUserRepository userRepository)
        {
            _messageRepository = messageRepository ?? throw new ArgumentNullException(nameof(messageRepository));
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public async Task<IActionResult> History([FromQuery] MessageFilterModel model = null)
        {
            if (model == null || model.IsEmpty)
            {
                var now = DateTimeProvider.Now;

                model = new MessageFilterModel
                {
                    From = now.AddHours(-1),
                    To = now
                };
            }

            model.Authors = await _userRepository.Get();

            Specification<Message> filter;

            if (model.AuthorId.HasValue)
            {
                filter = MessageSpecifications.ByAuthorWithAuthor(model.AuthorId.Value);
            }
            else
            {
                if (model.From == null)
                    ModelState.AddModelError(nameof(model.From), $"{nameof(model.From)} is null");

                if (model.To == null)
                    ModelState.AddModelError(nameof(model.To), $"{nameof(model.To)} is null");

                if (model.From == null || model.To == null)
                    return View(model);

                filter = MessageSpecifications.RangeWithAuthor(model.From.Value, model.To.Value);
            }

            model.Messages = await _messageRepository.Get(filter);


            return View(model);
        }
    }
}