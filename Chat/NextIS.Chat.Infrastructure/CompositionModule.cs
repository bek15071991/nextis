﻿using Autofac;
using NextIS.Chat.Core;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Infrastructure
{
   public class CompositionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();  
            builder.RegisterType<UnitOfWorkFactory>().As<IUnitOfWorkFactory>();  
            builder.RegisterType<UserRepository>().As<IUserRepository>();  
            builder.RegisterType<MessageRepository>().As<IMessageRepository>();  
        }
    }
}
