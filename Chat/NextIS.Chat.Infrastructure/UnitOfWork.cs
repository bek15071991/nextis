﻿using System;
using JetBrains.Annotations;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ChatDbContext _dbContext;

        public UnitOfWork([NotNull] ChatDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _dbContext.Database.BeginTransaction();
        }

        public void Dispose()
        { 
            _dbContext.Database.CurrentTransaction?.Rollback();
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
            _dbContext.Database.CurrentTransaction.Commit();
        }
    }
}