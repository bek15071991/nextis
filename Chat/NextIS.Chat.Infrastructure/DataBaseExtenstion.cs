﻿using Microsoft.EntityFrameworkCore;
using NextIS.Chat.Core;

namespace NextIS.Chat.Infrastructure
{
    public static class DataBaseExtenstion
    {
        public static void AddAdmin(this ModelBuilder builder)
        {
            builder.Entity<User>().HasData(new[]
            {
                new User(1, "admin", "password"),
            });
        } 
    }
}