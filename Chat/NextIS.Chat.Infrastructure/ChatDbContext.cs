﻿using Microsoft.EntityFrameworkCore;
using NextIS.Chat.Core;
using NextIS.Chat.Infrastructure.Configurations;

namespace NextIS.Chat.Infrastructure
{
    public class ChatDbContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }

        public DbSet<User> Users { get; set; }

        public ChatDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new MessageConfiguration());

            modelBuilder.AddAdmin();
        }
    }
}