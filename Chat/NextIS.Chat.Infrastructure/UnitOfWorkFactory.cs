﻿using System;
using JetBrains.Annotations;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Infrastructure
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly ChatDbContext _dbContext;

        public UnitOfWorkFactory([NotNull] ChatDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public IUnitOfWork Create()
        {
            return  new UnitOfWork(_dbContext);
        }
    }
}