﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using NextIS.Chat.Core;
using NextIS.Chat.Core.SeedWork;

namespace NextIS.Chat.Infrastructure
{
    public class MessageRepository : IMessageRepository
    {
        private readonly ChatDbContext _chatDbContext;

        public MessageRepository([NotNull] ChatDbContext chatDbContext)
        {
            _chatDbContext = chatDbContext ?? throw new ArgumentNullException(nameof(chatDbContext));
        }

        public async Task<IReadOnlyCollection<Message>> Get(ISpecification<Message> specification)
        {
            if (specification == null)
                throw new ArgumentNullException(nameof(specification));

            var queryableResultWithIncludes = specification.Includes
                .Aggregate(_chatDbContext.Set<Message>().AsQueryable(), (current, include) => current.Include(include));

            var secondaryResult = specification.IncludeStrings
                .Aggregate(queryableResultWithIncludes,
                    (current, include) => current.Include(include));

            return await secondaryResult.Where(specification.Criteria).ToListAsync();
        }

        public async Task Add(Message message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));
            await _chatDbContext.Messages.AddAsync(message);
        }
    }
}