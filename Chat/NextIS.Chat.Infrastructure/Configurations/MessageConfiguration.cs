﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NextIS.Chat.Core;

namespace NextIS.Chat.Infrastructure.Configurations
{
    public class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Text).HasMaxLength(255);
            
            builder.HasOne(c => c.Author).WithMany().HasForeignKey(o => o.AuthorId);
        }
    }
}