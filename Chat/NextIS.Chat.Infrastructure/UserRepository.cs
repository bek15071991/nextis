﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using NextIS.Chat.Core;

namespace NextIS.Chat.Infrastructure
{
    public class UserRepository : IUserRepository
    {
        private readonly ChatDbContext _chatDbContext;

        public UserRepository([NotNull] ChatDbContext chatDbContext)
        {
            _chatDbContext = chatDbContext ?? throw new ArgumentNullException(nameof(chatDbContext));
        }

        public async Task<User> Get(string login, string password)
        {
            if (login == null) throw new ArgumentNullException(nameof(login));
            if (password == null) throw new ArgumentNullException(nameof(password));

            return await _chatDbContext.Users.FirstOrDefaultAsync(u => u.Login == login && u.Password == password);
        }

        public async Task<IReadOnlyCollection<User>> Get()
        {
            return await _chatDbContext.Users.ToListAsync();
        }
    }
}